package com.techu.backend.repository;

import com.techu.apirest.model.ProductoModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductoRepository extends MongoRepository<ProductoModel, String> {
}
